# String Manipulation Module

The string manipulation module provides various functions for string manipulation, and a custom sprintf function. 

# Custom sprintf

The custom sprintf function accepts type input using `%FORMAT%` , where FORMAT is an abitrary-length string containing the format. It is possible to add custom formats by overloading the `sprintf_format_handler` function. 

sprintf reads the format string at compile-time to reduce overhead.

# Dependancies

None

# Use

Simply add the files to your build system

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
